import React from 'react';
import PropTypes from 'prop-types';

//import Content from 'content';
import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import {getModifiers} from 'libs/component';

import './Container.scss';

/**
 * Container
 * @description [Description]
 * @example
  <div id="Container"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Container, {
    	title : 'Example Container'
    }), document.getElementById("Container"));
  </script>
 */
const Container = props => {
	const componentClass = 'container';
	const {modifier, baseClass, size, children} = props;

	const content = typeof children === 'string' ? <Content content={children} /> : children;

	const className = getModifiers(componentClass, [modifier, size]);

	return <div className={`${className}${baseClass ? ` ${baseClass}__${componentClass}` : ''}`}>{content}</div>;
};

Container.defaultProps = {
	modifier: '',
	baseClass: '',
	children: '',
	size: null
};

Container.propTypes = {
	baseClass: PropTypes.string,
	children: PropTypes.node,
	modifier: PropTypes.string,
	size: PropTypes.string
};

export default Container;
